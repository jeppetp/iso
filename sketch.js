var width = window.innerWidth
var height = window.innerHeight

// euclidean distance
let dist = ((p1, p2) => sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2))

function windowResized() {
  width = windowWidth
  height = windowHeight
  resizeCanvas(width, height)
}

function setup() {
  createCanvas(windowWidth, windowHeight)
  colorMode(HSB, 255)
  frameRate(30)
  textSize(10);
  noStroke();

  // create sliders

  timedelaySlider = createSlider(0, 11, 3, 0);
  timedelaySlider.position(10, 10);
  timedelaySlider.style('width', '200px')

  chaosSlider = createSlider(1, 50, 10, 0);
  chaosSlider.position(10, 30);
  chaosSlider.style('width', '200px')

  sizeSlider = createSlider(1, 2000, 100, 0);
  sizeSlider.position(10, 50);
  sizeSlider.style('width', '200px')
  textAlign(CENTER)
  lsel = createSelect();
  lsel.style('width', '80px')
  lsel.style('height', '10px')
  lsel.position(10, 80);
  lsel.option('1');
  lsel.option('2');
  lsel.option('4');
  lsel.option('8');
  lsel.option('16');
  lsel.option('32');
  lsel.option('64');
  lsel.option('128');
  lsel.option('256');
  lsel.selected('32')

  ksel = createSelect();
  ksel.style('width', '80px')
  ksel.style('height', '10px')
  ksel.position(100, 80);
  ksel.option(1);
  ksel.option(2);
  ksel.option('4');
  ksel.option('8');
  ksel.option('16');
  ksel.option('32');
  ksel.option('64');
  ksel.option('128');
  ksel.option('256');
  ksel.selected('32')

  blendsel = createSelect();
  blendsel.style('width', '80px')
  blendsel.style('height', '10px')
  blendsel.position(10, 110);
  blendsel.option(BLEND);
  blendsel.option(ADD);
  blendsel.option(LIGHTEST);
  blendsel.option(DIFFERENCE);
  blendsel.option(EXCLUSION);
  blendsel.option(SCREEN);
  blendsel.option(OVERLAY);
  blendsel.option(HARD_LIGHT);
  blendsel.option(SOFT_LIGHT);
  blendsel.option(DODGE);
  blendsel.selected(DIFFERENCE)


}

function noiseMap(y, t, scale, k){
  var angle = map(y, 0, k, 0, TWO_PI)
  return [noise(scale*cos(angle)+1.11612,scale*sin(angle)+5.12315,t) * width,noise(scale*cos(angle)+123.11413, scale*sin(angle) + 1263.5611, t+13.1241)*height]
  // return [noise(scale*y+1.11612,scale*y+5.12315,t) * width,noise(scale*y+123.11413, scale*y + 1263.5611, t+13.1241)*height]

}

let fg = [190, 100, 100]
let bg = [0,10 , 10]
// let blobcolor = [190, 180, 100]
// var blob = [0, 0]
// var blobdir = Math.PI
// var blobnoise = 1
// var blobturnspeed = 0.10
// var blobspeed = 0.5
var t = 0
function draw() {
  const l = int(lsel.value())+1
  const k = ksel.value()
  const timedelay = timedelaySlider.value()
  const chaos = chaosSlider.value()
  const size = sizeSlider.value()
  blendMode(REPLACE)
  background(...bg)
  blendMode(blendsel.value())
  for (i = 0; i < l; i++){
    for (j = 0; j < k; j++){
      // strokeWeight(1)
      // stroke(0,0,0)
      noStroke()
      fill(map(j, 0,k,0,255), 255, 255)
      circle(...noiseMap(j, t + i/(2**timedelay), chaos/100, k),map(i,0,l,0,size))
    }
  }
  t+= 0.005
  // t+=noise(t*10)*0.01
}
